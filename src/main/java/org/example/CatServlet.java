package org.example;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/cat")
public class CatServlet extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter writer = response.getWriter();

        String name = "Мурзик";

        int weight = 5;

        boolean isAngry = true;

        String zlost = (isAngry ?  "Сердитый" :  "Дружелюбный");

        writer.write(String.format
                ("<h1> Hello, я %s, я очень %s, потому что меня не кормят и я вешу всего %d</h>",
                        name, zlost, weight));
        writer.close();
    }
}
